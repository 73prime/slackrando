package main

import (
	"fmt"
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"os"
	"strings"
	"testing"
)

func copyFile(in, out string) (int64, error) {
	i, e := os.Open(in)
	if e != nil {
		return 0, e
	}
	defer i.Close()
	o, e := os.Create(out)
	if e != nil {
		return 0, e
	}
	defer o.Close()
	return o.ReadFrom(i)
}

func setupMain() {
	//copy the textfixture to keep it safe for later
	in := fmt.Sprintf("%s/randomphrases.json", os.Getenv("DATA_DIRECTORY"))
	out := fmt.Sprintf("%s/randomphrases_orig.json", os.Getenv("DATA_DIRECTORY"))
	_, _ = copyFile(in, out)
}
func teardownMain() {
	//copy textfixture back
	in := fmt.Sprintf("%s/randomphrases_orig.json", os.Getenv("DATA_DIRECTORY"))
	out := fmt.Sprintf("%s/randomphrases.json", os.Getenv("DATA_DIRECTORY"))
	os.Remove(out)
	copyFile(in, out)
	os.Remove(in)
}
func TestMain(m *testing.M) {
	setupMain()
	code := m.Run()
	teardownMain()
	os.Exit(code)
}
func TestPhraseHandler(t *testing.T) {

	token := os.Getenv("VALID_TOKEN")
	//testing POST-only
	req := httptest.NewRequest(http.MethodGet, "/addphrase?token="+token, nil)
	w := httptest.NewRecorder()
	AddPhraseHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status error %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing bad token
	req = httptest.NewRequest(http.MethodPost, "/?token=bad", nil)
	w = httptest.NewRecorder()
	AddPhraseHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusUnauthorized {
		t.Errorf("Expected Status error %v, but got %v", http.StatusUnauthorized, res.StatusCode)
	}

	//testing happypath
	payload := strings.NewReader(`{
		       	"text" : "Test String"
		}`)
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, payload)
	w = httptest.NewRecorder()
	AddPhraseHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	expected := "{\"response_type\":\"in_channel\",\"text\":\"Successfully added phrase.\"}"
	if string(data) != expected {
		t.Errorf("expected %v got %v", expected, string(data))
	}
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected Status error %v, but got %v", http.StatusOK, res.StatusCode)
	}
	//testing double add
	//AddPhraseHandler(w, req)
	//res = w.Result()
	//defer res.Body.Close()
	//data, err = ioutil.ReadAll(res.Body)
	//if err != nil {
	//	t.Errorf("expected error to be nil got %v", err)
	//}
	//if res.StatusCode != http.StatusOK {
	//	t.Errorf("Expected Status error %v, but got %v", http.StatusOK, res.StatusCode)
	//}
	//expected = "{\"response_type\":\"in_channel\",\"text\":\"Successfully added phrase.\"}"
	//if string(data) != expected {
	//	t.Errorf("expected %v got %v", expected, string(data))
	//}
}
func TestRootHandler(t *testing.T) {
	//testing POST-only
	token := os.Getenv("VALID_TOKEN")
	req := httptest.NewRequest(http.MethodGet, "/?token="+token, nil)
	w := httptest.NewRecorder()
	RootHandler(w, req)
	res := w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status error %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing empty header
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, nil)
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status error %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing empty data, but bad header
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, nil)
	req.Header.Add("Content-Type", "application/jsonmisspelled")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status code %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}

	//testing JSON data
	payload := strings.NewReader(`{
    	"text" : "Test String"
	}`)
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, payload)
	req.Header.Add("Content-Type", "application/json")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusInternalServerError {
		t.Errorf("Expected Status code %v, but got %v", http.StatusInternalServerError, res.StatusCode)
	}
	data, err := ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	expected := "{\"response_type\":\"in_channel\",\"text\":\"Prompt Test String not implemented.\"}"
	if string(data) != expected {
		t.Errorf("expected %v got %v", expected, string(data))
	}

	//testing bigbang
	payload = strings.NewReader(`text=bigbang`)
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, payload)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected Status code %v, but got %v", http.StatusOK, res.StatusCode)
	}
	data, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}

	if len(string(data)) == 0 {
		t.Errorf("Expected > 0 length, got 0")
	}

	//testing random string
	payload = strings.NewReader(`text=string`)
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, payload)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected Status code %v, but got %v", http.StatusOK, res.StatusCode)
	}
	data, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if len(string(data)) == 0 {
		t.Errorf("Expected > 0 length, got 0")
	}

	//testing random diceroll
	payload = strings.NewReader(`text=dice`)
	req = httptest.NewRequest(http.MethodPost, "/?token="+token, payload)
	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	w = httptest.NewRecorder()
	RootHandler(w, req)
	res = w.Result()
	defer res.Body.Close()
	if res.StatusCode != http.StatusOK {
		t.Errorf("Expected Status code %v, but got %v", http.StatusOK, res.StatusCode)
	}
	data, err = ioutil.ReadAll(res.Body)
	if err != nil {
		t.Errorf("expected error to be nil got %v", err)
	}
	if len(string(data)) == 0 {
		t.Errorf("Expected > 0 length, got 0")
	}

}
