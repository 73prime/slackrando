package main

import (
	"encoding/json"
	"errors"
	"fmt"
	log "github.com/sirupsen/logrus"
	"math/rand"
	"net/http"
	"os"
	"strconv"
	"strings"
	"time"
)

//see https://transform.tools/json-to-go
type BigBang struct {
	Episodes []struct {
		Season     string `json:"Season"`
		Title      string `json:"Title"`
		Episode    int    `json:"Episode"`
		FirstAired string `json:"FirstAired"`
	} `json:"Episodes"`
}
type RandomItem struct {
	Items []string `json:"Items"`
}

var port int
var datadirectory, validtoken string

var episodes BigBang
var randomitems RandomItem

func loadEpisodes() (int, error) {
	fileBytes, err := os.ReadFile(fmt.Sprintf("%s/bigbang.json", datadirectory))
	if err != nil {
		return 0, err
	} else {
		err = json.Unmarshal(fileBytes, &episodes)
		if err != nil {
			return 0, err
		}
	}
	return len(episodes.Episodes), nil
}
func loadRandomPhrases() (int, error) {
	f := fmt.Sprintf("%s/%s", datadirectory, "randomphrases.json")
	log.Infof("Using %s as phrase file.", f)
	fileBytes, err := os.ReadFile(f)
	if err != nil {
		return 0, err
	} else {
		err = json.Unmarshal(fileBytes, &randomitems)
		if err != nil {
			return 0, err
		}
	}
	return len(randomitems.Items), nil
}
func init() {
	initlogging()
	initConfig()
	total, err := loadEpisodes()
	if err != nil {
		log.Errorf("Error loading episodes: %s", err)
	} else {
		log.Infof("Loaded %d episodes from disk.", total)
		total, err = loadRandomPhrases()
		if err != nil {
			log.Errorf("Error loading episodes: %s", err)
		} else {
			log.Infof("Loaded %d random phrases from disk.", total)
		}
	}
}
func initlogging() {
	//Trace, Debug, Info, Warn, Error, Fatal, and Panic are valid
	logfmt := os.Getenv("LOG_FORMAT")
	if strings.ToUpper(logfmt) == "JSON" {
		log.SetFormatter(&log.JSONFormatter{})
	} else {
		log.SetFormatter(&log.TextFormatter{
			DisableColors: false,
			FullTimestamp: true,
		})
	}
	lvl, ok := os.LookupEnv("LOG_LEVEL")
	// LOG_LEVEL not set, let's default to debug
	if !ok {
		lvl = "debug"
	}
	// parse string, this is built-in feature of logrus
	ll, err := log.ParseLevel(lvl)
	if err != nil {
		ll = log.DebugLevel
	}
	// set global log level
	log.SetLevel(ll)
	log.Debug("Finished configuring logger.")
}
func initConfig() {
	port = 5000
	portstr := os.Getenv("PORT")
	porti, err := strconv.Atoi(portstr)
	if err == nil {
		port = porti
	}

	datadirectory = os.Getenv("DATA_DIRECTORY")
	if datadirectory == "" {
		log.Fatalf("DATA_DIRECTORY should be an envvar.")
	} else {
		_, err := os.Stat(datadirectory)
		if err != nil || os.IsNotExist(err) {
			log.Fatalf("DATA_DIRECTORY %s does not exist.", datadirectory)
		}
	}

	validtoken = os.Getenv("VALID_TOKEN")
	if validtoken == "" {
		log.Fatalf("VALID_TOKEN should be an envvar.")
	}
	log.Debugf("Finished initConfig, port %d, datadirectory %s, and a valid token.", port, datadirectory)
}
func WriteSlackResponse(w http.ResponseWriter, message string, statusCode int) {
	data, _ := json.Marshal(map[string]string{
		"response_type": "in_channel",
		"text":          message,
	})
	w.Header().Set("Content-Type", "application/json")
	w.WriteHeader(statusCode)
	w.Write(data)
	log.Infof("Sent `%s` back to caller.", message)
}
func AddPhraseHandler(w http.ResponseWriter, r *http.Request) {
	if !ValidCaller(r) {
		WriteSlackResponse(w, "Token not valid", http.StatusUnauthorized)
		return
	}
	if r.Method != "POST" {
		message := fmt.Sprintf("Verb %s not implemented.", r.Method)
		WriteSlackResponse(w, message, http.StatusInternalServerError)
		return
	}
	prompt := r.FormValue("text")
	if prompt == "" {
		//need to see if JSON is a thing.
		//curl -H "Content-Type: application/json" http://127.0.0.1:5000/?token=blah -d '{"text":"new phrase here"}'
		type prompt_struct struct {
			Text string `json:"text"`
		}
		decoder := json.NewDecoder(r.Body)
		var p prompt_struct
		err := decoder.Decode(&p)
		if err == nil {
			prompt = p.Text
			log.Infof("Prompt via JSON payload is: %s.", prompt)
		}
	}
	log.Infof("Will store phrase: %s.\n", prompt)
	err := addPhrase(prompt)
	if err == nil {
		WriteSlackResponse(w, "Successfully added phrase.", http.StatusOK)
	} else {
		WriteSlackResponse(w, fmt.Sprintf("Error: %s", err), http.StatusInternalServerError)
	}
}
func doesPhraseExist(phrase string) bool {
	for _, v := range randomitems.Items {
		if strings.ToUpper(v) == strings.ToUpper(phrase) {
			return true
		}
	}
	return false
}
func addPhrase(phrase string) error {
	if doesPhraseExist(phrase) {
		errorString := fmt.Sprintf("Phrase %s already exists in list.", phrase)
		log.Errorf(errorString)
		return errors.New(errorString)
	}
	//doesn't exist, let's add it.
	randomitems.Items = append(randomitems.Items, phrase)
	//now flush to file
	json, _ := json.Marshal(randomitems)
	f, _ := os.Create(fmt.Sprintf("%s/%s", datadirectory, "randomphrases.json"))
	_, err := f.Write(json)
	return err
}
func ValidCaller(r *http.Request) bool {
	query := r.URL.Query()
	token, present := query["token"] //filters=["color", "price", "brand"]
	if !present || len(token) == 0 || token[0] != validtoken {
		return false
	}
	return true
}

func RootHandler(w http.ResponseWriter, r *http.Request) {
	if !ValidCaller(r) {
		WriteSlackResponse(w, "Token not valid", http.StatusUnauthorized)
		return
	}
	if r.Method != "POST" {
		message := fmt.Sprintf("Verb %s not implemented.", r.Method)
		WriteSlackResponse(w, message, http.StatusInternalServerError)
		return
	}
	//this works with curl from slack in the form of:
	//curl http://127.0.0.1:5000/ -d "text=bigbang"
	prompt := r.FormValue("text")
	if prompt == "" {
		//need to see if JSON is a thing.
		//curl -H "Content-Type: application/json" http://127.0.0.1:5000/?token=blah -d '{"text":"bigbang"}'
		type prompt_struct struct {
			Text string `json:"text"`
		}
		decoder := json.NewDecoder(r.Body)
		var p prompt_struct
		err := decoder.Decode(&p)
		if err == nil {
			prompt = p.Text
			log.Infof("Prompt via JSON payload is: %s.", prompt)
		}
	}
	log.Infof("Will use prompt: %s.\n", prompt)
	var text string
	switch prompt {
	case "bigbang":
		text = getRandomEpisodeTitle()
	case "string":
		text, _ = getRandomString(20)
	case "agendaitem":
		text = getRandomPhrase()
	case "dice":
		text = getRandomDiceRoll(6)
	case "8ball":
		text = get8Ball()
	default:
		message := fmt.Sprintf("Prompt %s not implemented.", prompt)
		WriteSlackResponse(w, message, http.StatusInternalServerError)
		return
	}
	WriteSlackResponse(w, text, http.StatusOK)
}
func getRandomEpisodeTitle() string {
	randint := rand.Intn(len(episodes.Episodes))
	return episodes.Episodes[randint].Title
}
func getRandomPhrase() string {
	randint := rand.Intn(len(randomitems.Items))
	return randomitems.Items[randint]
}
func getRandomString(l int) (string, error) {
	bytes := make([]byte, l)
	for i := 0; i < l; i++ {
		bytes[i] = byte(randInt(65, 90))
	}
	return string(bytes), nil
}
func get8Ball() string {
	poss := make([]string, 20)
	poss[0] = "It is certain"
	poss[1] = "It is decidedly so"
	poss[2] = "Without a doubt"
	poss[3] = "Yes definitely"
	poss[4] = "You may rely on it"
	poss[5] = "As I see it, yes"
	poss[6] = "Most likely"
	poss[7] = "Outlook good"
	poss[8] = "Yes"
	poss[9] = "Signs point to yes"
	poss[10] = "Reply hazy, try again"
	poss[11] = "Ask again later"
	poss[12] = "Better not tell you now"
	poss[13] = "Cannot predict now"
	poss[14] = "Concentrate and ask again"
	poss[15] = "Don't count on it"
	poss[16] = "My reply is no"
	poss[17] = "My sources say no"
	poss[18] = "Outlook not so good"
	poss[19] = "Very doubtful"
	ri := randInt(0, 19)
	result := fmt.Sprintf("Your magic 8-ball response: %s", poss[ri])
	return result
}
func getRandomDiceRoll(len int) string {
	//Unicode 2680 = 1; 2685 = 6
	possDie := make([]rune, 6)
	possDie[0] = '⚀'
	possDie[1] = '⚁'
	possDie[2] = '⚂'
	possDie[3] = '⚃'
	possDie[4] = '⚄'
	possDie[5] = '⚅'
	result := ""
	for i := 0; i < len; i++ {
		die := randInt(0, 5)
		result += fmt.Sprintf("%s=(%d) ", string(possDie[die]), die+1)
	}
	return strings.TrimSpace(result)
}
func randInt(min int, max int) int {
	return min + rand.Intn(max-min)
}
func handleLocalRequests() {
	http.HandleFunc("/", RootHandler)
	http.HandleFunc("/addphrase", AddPhraseHandler)
	log.Printf("Started Webserver on port %d.\n", port)
	log.Fatal(http.ListenAndServe(fmt.Sprintf(":%d", port), nil))
}
func main() {
	log.Info("slackrando API started")
	rand.Seed(time.Now().UnixNano())
	handleLocalRequests()
}
