# What is this?
It's a collection of randomizer type things for Slack slash-command.

### Run locally with Docker
* Ensure you have docker: `sudo apt-get install docker.io`, for example.
* Ensure your user has access: `sudo usermod -aG docker $USER`
* Create `.env` file:
```env
#!/bin/bash
export VALID_TOKEN="blah"
```
* Source it: `source .env`
* Run `make docker`
* Open another terminal, test it: `curl -X POST http://127.0.0.1:5000\?token\=blah -d "text=string"`
