.PHONY: default
default: displayhelp ;

displayhelp:
	@echo Use "clean, showcoverage, tests, build, docker or run" with make, por favor.

showcoverage: tests
	@echo Running Coverage output
	go tool cover -html=coverage.out

tests: clean
	@echo Running Tests
	DATA_DIRECTORY=${PWD}/data VALID_TOKEN=blah go test --coverprofile=coverage.out ./...

run: build
	@echo Running program
	LOG_LEVEL=DEBUG ./bin/slackrando

build: clean
	@echo Running build command
	go build -o bin/slackrando src/main.go

clean:
	@echo Removing binary TODO
	rm -rf ./bin ./vendor Gopkg.lock

docker:
	docker build -t slackrando:latest . -f Dockerfile
	docker run -it -e PORT=5000 -e DATA_DIRECTORY=/app/data/ -e VALID_TOKEN=${VALID_TOKEN} -p 5000:5000 slackrando:latest

